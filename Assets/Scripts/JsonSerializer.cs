using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class JsonSerializer
{
    public static Hero JsonRead(string json)
    {
        return JsonConvert.DeserializeObject<Hero>(json);
    }
}

public partial class Hero
    {
        [JsonProperty("success")]
        public bool success { get; set; }

        [JsonProperty("data")]
        public Data data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("tokenID")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public long tokenId { get; set; }

        [JsonProperty("matronID")]
        public long matronId { get; set; }

        [JsonProperty("sireID")]
        public long sireId { get; set; }

        [JsonProperty("genes")]
        public string genes { get; set; }

        [JsonProperty("birthtime")]
        public long birthtime { get; set; }

        [JsonProperty("coolDownEndTime")]
        public long coolDownEndTime { get; set; }

        [JsonProperty("siringWithID")]
        public long siringWithId { get; set; }

        [JsonProperty("generation")]
        public long generation { get; set; }

        [JsonProperty("era")]
        public long era { get; set; }

        [JsonProperty("level")]
        public long lLevel { get; set; }

        [JsonProperty("class")]
        public long Class { get; set; }

        [JsonProperty("skills")]
        public long[] skills { get; set; }

        [JsonProperty("api:")]
        public Api api { get; set; }
    }

    public partial class Api
    {
        [JsonProperty("username")]
        public string username { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }

        [JsonProperty("external_url")]
        public Uri externalUrl { get; set; }

        [JsonProperty("image")]
        public Uri image { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("token")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public long token { get; set; }

        [JsonProperty("matches")]
        public Match[] matches { get; set; }

        [JsonProperty("attributes")]
        public Attribute[] attributes { get; set; }
    }

    public partial class Attribute
    {
        [JsonProperty("trait_type")]
        public string traitType { get; set; }

        [JsonProperty("value")]
        public string value { get; set; }
    }

    public partial class Match
    {
        [JsonProperty("id")]
        public long id { get; set; }

        [JsonProperty("hero")]
        public long hero { get; set; }

        [JsonProperty("opponent")]
        public long opponent { get; set; }

        [JsonProperty("winner")]
        public long winner { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset createdAt { get; set; }
    }

    

