﻿using UnityEngine;
using System.Collections;
using System.IO;

/*
Usage:
1. Attach this script to your chosen camera's game object.
2. Set that camera's Clear Flags field to Solid Color.
3. Use the inspector to set frameRate and framesToCapture
4. Choose your desired resolution in Unity's Game window (must be less than or equal to your screen resolution)
5. Turn on "Maximise on Play"
6. Play your scene. Screenshots will be saved to YourUnityProject/Screenshots by default.
*/

public class ScreenshotTransparent : MonoBehaviour
{
    #region public fields
    [Tooltip("A folder will be created with this base name in your project root")]
    public string folderBaseName = "Screenshots";
    [Tooltip("How many frames should be captured per second of game time")]
    public int frameRate = 24;
    [Tooltip("How many frames should be captured before quitting")]
    public int framesToCapture = 24;
    [Tooltip("Output of base64")]
    #endregion

    #region private fields
    private string folderName = "";
    private GameObject whiteCamGameObject, blackCamGameObject;
    private Camera mainCam, whiteCam, blackCam;
    private int videoFrame = 0; // how many frames we've rendered
    private float originalTimescaleTime;
    private bool done = true;
    private float screenWidth, screenHeight;
    private Texture2D textureBlack, textureWhite, textureTransparentBackground;
    private string _base64output;
    #endregion

    public string base64output
    {
        get { return _base64output; }
    }

    private void Awake() 
    {
        mainCam = gameObject.GetComponent<Camera>();
    }

    private void Init()
    {
        CreateBlackAndWhiteCameras();
        CreateNewFolderForScreenshots();
        CacheAndInitialiseFields();
        Time.captureFramerate = frameRate;
    }

    public void MakeScreenshot()
    {
        Init();
        done = false;
    }

    private void LateUpdate()
    {
        if (!done)
        {
            StartCoroutine(CaptureFrame());

            if (done)
            {
                //Debug.Log("Complete! " + videoFrame + " videoframes rendered. File names are 0 indexed)");
            }
        }
        else
        {
            //Debug.Log("Complete! " + videoFrame + " videoframes rendered. File names are 0 indexed)");
            //Debug.Break();
        }
    }

    private IEnumerator CaptureFrame()
    {
        yield return new WaitForEndOfFrame();
        if (videoFrame < framesToCapture)
        {
            RenderCamToTexture(blackCam, textureBlack);
            RenderCamToTexture(whiteCam, textureWhite);
            CalculateOutputTexture();

            SavePNG();
            SaveBase64();

            videoFrame++;
            //Debug.Log("Rendered frame " + videoFrame);
            videoFrame++;
        }
        else
        {
            done = true;
            StopCoroutine("CaptureFrame");
        }
    }

    private void RenderCamToTexture(Camera cam, Texture2D tex)
    {
        cam.enabled = true;
        cam.Render();
        WriteScreenImageToTexture(tex);
        cam.enabled = false;
    }

    private void CreateBlackAndWhiteCameras()
    {
        whiteCamGameObject = (GameObject)new GameObject();
        whiteCamGameObject.name = "White Background Camera";
        whiteCam = whiteCamGameObject.AddComponent<Camera>();
        whiteCam.CopyFrom(mainCam);
        whiteCam.backgroundColor = Color.white;
        whiteCamGameObject.transform.SetParent(gameObject.transform, true);

        blackCamGameObject = (GameObject)new GameObject();
        blackCamGameObject.name = "Black Background Camera";
        blackCam = blackCamGameObject.AddComponent<Camera>();
        blackCam.CopyFrom(mainCam);
        blackCam.backgroundColor = Color.black;
        blackCamGameObject.transform.SetParent(gameObject.transform, true);
    }

    private void CreateNewFolderForScreenshots()
    {
        // Find a folder name that doesn't exist yet. Append number if necessary.
        folderName = folderBaseName;
        int count = 1;
        if (System.IO.Directory.Exists(folderName))
        {
            return;
        }

        else 
        {
            folderName = folderBaseName + count;
            count++;
        }

        System.IO.Directory.CreateDirectory(folderName); // Create the folder
    }

    private void CalculateOutputTexture()
    {
        Color color;
        for (int y = 0; y < textureTransparentBackground.height; ++y)
        {
            // each row
            for (int x = 0; x < textureTransparentBackground.width; ++x)
            {
                // each column
                float alpha = textureWhite.GetPixel(x, y).r - textureBlack.GetPixel(x, y).r;
                alpha = 1.0f - alpha;
                if (alpha == 0)
                {
                    color = Color.clear;
                }
                else
                {
                    color = textureBlack.GetPixel(x, y) / alpha;
                }
                color.a = alpha;
                textureTransparentBackground.SetPixel(x, y, color);
            }
        }
    }

    private void SavePNG()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
        string name = string.Format("{0}/{1:D04} shot.png", folderName, videoFrame);
        var pngShot = textureTransparentBackground.EncodeToPNG();
        File.WriteAllBytes(name, pngShot);
#endif
    }

    private void SaveBase64()
    {
        string name = string.Format("{0}/{1:D04} shot.png", folderName, videoFrame);
        //var pngShot = textureTransparentBackground.EncodeToPNG();
        _base64output = TeamUtilityEditor.TextureToBase64Tool.Texture2DToBase64(textureTransparentBackground);
    }

    private void WriteScreenImageToTexture(Texture2D tex)
    {
        tex.ReadPixels(new Rect(Screen.width / 2.5f, Screen.height / 13f, screenWidth, screenHeight), 0, 0);
        tex.Apply();
    }

    private void CacheAndInitialiseFields()
    {
        originalTimescaleTime = Time.timeScale;
        //screenWidth = 140;
        //screenHeight = 370;
        screenWidth = Screen.width / 5f;
        screenHeight = Screen.height / 1.21f;
        
        //Debug.Log("Width:" + screenWidth + " Height: " + screenHeight);

        textureBlack = new Texture2D((int)screenWidth, (int)screenHeight, TextureFormat.ARGB32, false);
        textureWhite = new Texture2D((int)screenWidth, (int)screenHeight, TextureFormat.ARGB32, false);
        textureTransparentBackground = new Texture2D((int)screenWidth, (int)screenHeight, TextureFormat.ARGB32, false);
    }
}