﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Resource", menuName = "Resources/HairColor")]
public class ResourcesHairsColors : ScriptableObject
{
    public int[] indx;
    public Color hairColor;
}
