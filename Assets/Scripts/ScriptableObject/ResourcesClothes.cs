﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Resource", menuName = "Resources/Clothes")]
public class ResourcesClothes : ScriptableObject
{
    public GameObject[] clothes;
}
