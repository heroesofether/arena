﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Resource", menuName = "Resources/Item")]
public class Resources3D : ScriptableObject
{
    public int[] indx;
    
    public Material mat;
}
