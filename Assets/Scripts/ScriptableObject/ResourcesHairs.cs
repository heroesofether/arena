﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Resource", menuName = "Resources/Hair")]

public class ResourcesHairs : ScriptableObject
{
    public int[] indx;
    public GameObject hair;
}
