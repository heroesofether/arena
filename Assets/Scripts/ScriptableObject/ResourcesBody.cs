﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ResourceBody", menuName = "Resources/ItemBody")]
public class ResourcesBody : ScriptableObject
{
    public Material[] materials;
}
