﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Resource", menuName = "Resources/SkinColor")]
public class ResourcesSkinColors : ScriptableObject
{
    public int[] indx;
    public Color skinColor;
}
