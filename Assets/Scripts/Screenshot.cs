﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Screenshot : MonoBehaviour
{
    private void SaveScreenshot()
    {
        StartCoroutine(SaveScreenshot_ReadPixelsAsync());
    }

    private IEnumerator SaveScreenshot_ReadPixelsAsync()
    {
        //Wait for graphics to render
        yield return new WaitForEndOfFrame();

        //Create a texture to pass to encoding
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

        //Put buffer into texture
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();

        //Split the process up--ReadPixels() and the GetPixels() call inside of the encoder are both pretty heavy
        yield return 0;

        byte[] bytes = texture.EncodeToPNG();
        var image = bytes;

        // For testing purposes, also write to a file in the project folder
        File.WriteAllBytes(Application.dataPath + "/SavedScreenshot.png", image);

        //var name = GetFileHelper();
        //PostObject(name);
        var path = Application.dataPath + "/SavedScreenshot.png";

        //UploadObjectForBucket(path, S3BucketName, fileNameOnBucket);

        //Tell unity to delete the texture, by default it seems to keep hold of it and memory crashes will occur after too many screenshots.
        Destroy(texture);
    }


    private string GetFileHelper()
    {
        var fileName = "MEGANAME322";

        if (!File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + fileName))
        {
            var streamReader = File.CreateText(Application.persistentDataPath + Path.DirectorySeparatorChar + fileName);
            streamReader.WriteLine("This is a sample s3 file uploaded from unity s3 sample");
            streamReader.Close();
        }
        return fileName;
    }
}