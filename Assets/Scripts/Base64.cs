﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Base64 : MonoBehaviour
{
    public string encodedText;
    public string outputText;
    public Image image;
    public RawImage rawImg;
    private Image pictureInScene;
    private Image s;

    private void Start()
    {
        byte[] decodedBytes = Convert.FromBase64String (encodedText);
        string decodedText = Encoding.UTF8.GetString (decodedBytes);


        /* var tex = TeamUtilityEditor.TextureToBase64Tool.Base64ToTexture2D(encodedText);
        rawImg.texture = tex;
        
        var b = tex.EncodeToPNG();
        Texture2D img = new Texture2D(300, 300);
        img.LoadImage(b);
        img.Apply();
        //rawImg.texture = img;

        //image.sprite.texture.LoadImage(b);
       // image.sprite.texture.Apply(); */

       SaveScreenshot();
    }

    public void SaveScreenshot()
    {
        StartCoroutine(SaveScreenshot_ReadPixelsAsynch());
    }

    private IEnumerator SaveScreenshot_ReadPixelsAsynch()
    {
        //Wait for graphics to render
        yield return new WaitForEndOfFrame();

        //Create a texture to pass to encoding
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

        //Put buffer into texture
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();

        //Split the process up--ReadPixels() and the GetPixels() call inside of the encoder are both pretty heavy
        yield return 0;

        byte[] bytes = texture.EncodeToPNG();
        var image = bytes;
        
        //var s = TeamUtilityEditor.TextureToBase64Tool.Texture2DToBase64(texture);
        //outputText = s;

        //var tex = TeamUtilityEditor.TextureToBase64Tool.Base64ToTexture2D(s);
        //rawImg.texture = tex;

#if !UNITY_WEBGL
        // For testing purposes, also write to a file in the project folder
        File.WriteAllBytes(Application.dataPath + "/SavedScreenshot.png", image);


        //var name = GetFileHelper();
        //PostObject(name);
        var path = Application.dataPath + "/SavedScreenshot.png";
#endif


        //Tell unity to delete the texture, by default it seems to keep hold of it and memory crashes will occur after too many screenshots.
        Destroy(texture);
    }
}
