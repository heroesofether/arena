﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ModelViewer : MonoBehaviour
{
    [SerializeField] private bool turnOnViewer;

    private GameObject player = null;
    private float rotateSpeed = 20;
    private float wheelSpeed = 40;
    private float rotateSpeedModifier = 100;
    private Vector3 mPrevPos = Vector3.zero;
    private Vector3 mPosDetla = Vector3.zero;
    private bool isView;

    private GenomeManager gManager;
    private WebRequest wRequest;
    private Camera mCamera;

    private void Awake()
    {
        gManager = GetComponent<GenomeManager>();
        mCamera = Camera.main.GetComponent<Camera>();
        wRequest = GetComponent<WebRequest>();
    }

    private void Start()
    {
        if (turnOnViewer)
            View(6); // Call to start viewer
    }

    private void Update()
    {
        Rotator360();
    }

    public void View(int indexID)
    {
        //StartCoroutine(wRequest.OnResponceViewer(indexID, callbackFunc));
        //StartCoroutine(OnResponceViewer(indexID));

        StartCoroutine(wRequest.OnResponceViewer(indexID, gManager.GeneratorWithSubstring, OnViewerLoaded));
    }

    private void OnViewerLoaded(bool done)
    {
        if (done)
            isView = true;

        else
            Debug.Log("There was an error with " + gameObject.name);
    }

    private void Rotator360()
    {
        if (isView)
        {
            if (!player)
            {
                player = gManager.currentPlayer;
                return;
            }

            if (Input.GetMouseButton(0))
            {
                mPosDetla = Input.mousePosition - mPrevPos;
                if (Vector3.Dot(player.transform.up, Vector3.up) >= 0)
                {
                    player.transform.Rotate(player.transform.up, -Vector3.Dot(mPosDetla, Camera.main.transform.right) * rotateSpeed * Time.deltaTime, Space.World);
                    //transform.RotateAround(point.position, transform.up, -Vector3.Dot(mPosDetla, Camera.main.transform.right));
                }

                else
                {
                    player.transform.Rotate(player.transform.up, Vector3.Dot(mPosDetla, Camera.main.transform.right) * rotateSpeed * Time.deltaTime, Space.World);
                    //transform.RotateAround(point.position, transform.up, Vector3.Dot(mPosDetla, Camera.main.transform.right));
                }

                player.transform.Rotate(Camera.main.transform.right, Vector3.Dot(mPosDetla, Camera.main.transform.up) * rotateSpeed * Time.deltaTime, Space.World);
                //transform.RotateAround(point.position, Camera.main.transform.right, Vector3.Dot(mPosDetla, Camera.main.transform.right));
            }

            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0f && mCamera.fieldOfView >= 34f)
                {
                    mCamera.fieldOfView = mCamera.fieldOfView - 2 * wheelSpeed * Time.deltaTime;
                    mCamera.transform.position = mCamera.transform.position + new Vector3(0, 0.015f, 0) * wheelSpeed * Time.deltaTime;
                }

                if (Input.GetAxis("Mouse ScrollWheel") < 0f && mCamera.fieldOfView <= 60f)
                {
                    mCamera.fieldOfView = mCamera.fieldOfView + 2 * wheelSpeed * Time.deltaTime;
                    mCamera.transform.position = mCamera.transform.position - new Vector3(0, 0.015f, 0) * wheelSpeed * Time.deltaTime;
                }
            }

            else if (!Input.GetMouseButton(0))
                player.transform.Rotate(0, -rotateSpeed * Time.deltaTime, 0);

            mPrevPos = Input.mousePosition;
        }
    }

    private void RotateWithMouse()
    {
        var mouse = Camera.main.ScreenPointToRay(Input.mousePosition);
        var forward = transform.TransformDirection(Vector3.forward);

        RaycastHit hit;
        GameObject player;

        if (Physics.Raycast(mouse, out hit, Mathf.Infinity))
        {
            //Debug.DrawRay(mouse, forward * hit.distance, Color.blue);

            player = hit.collider.gameObject;

            Debug.Log("Touched it");

            var rotationY = Quaternion.Euler(0f, -mouse.origin.x * rotateSpeedModifier, 0f);

            transform.rotation = rotationY * transform.rotation;

            //transform.rotation = Quaternion.Slerp(transform.rotation, rotationY, Time.deltaTime * rotateSpeedModifier);
        }
    }

    private void ColliderRot()
    {
        if (Input.GetMouseButton(0))
        {
            mPosDetla = Input.mousePosition - mPrevPos;
            if (Vector3.Dot(transform.up, Vector3.up) >= 0)
            {
                transform.Rotate(transform.up, -Vector3.Dot(mPosDetla, Camera.main.transform.right), Space.World);
                //transform.RotateAround(point.position, transform.up, -Vector3.Dot(mPosDetla, Camera.main.transform.right));
            }

            else
            {
                transform.Rotate(transform.up, Vector3.Dot(mPosDetla, Camera.main.transform.right), Space.World);
                //transform.RotateAround(point.position, transform.up, Vector3.Dot(mPosDetla, Camera.main.transform.right));
            }

            /*Debug.Log("Dot " + x);
          Debug.Log("Camera main: " + Camera.main.transform.up);
          Debug.Log("PosDelta " + mPosDetla);
          Vector3 v = new Vector3(0,2,0); */

            //Debug.Log(transform.position);
            var world = transform.TransformPoint(transform.position);

            Debug.Log(world);

            //if (Mathf.Abs(world.z) < 1)
            transform.Rotate(Camera.main.transform.right, Vector3.Dot(mPosDetla, Camera.main.transform.up), Space.World);
            //transform.RotateAround(point.position, Camera.main.transform.right, Vector3.Dot(mPosDetla, Camera.main.transform.right));
        }

        else transform.Rotate(0, -rotateSpeed * Time.deltaTime, 0);

        mPrevPos = Input.mousePosition;
    }
}
