﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HoE.Utils
{
    public class Settings : MonoBehaviour
    {
        //[SerializeField] private bool isLog;
        //[SerializeField] private bool isRunInBackground;
        private void Awake()
        {
            //Debug.unityLogger.logEnabled = isLog;

            #if UNITY_EDITOR
                Debug.unityLogger.logEnabled = true;
            #else
                Debug.unityLogger.logEnabled = false;
            #endif
        }
    }
}
