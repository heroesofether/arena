﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using UnityEngine;
using UnityEngine.UI;

public class AWSManager : MonoBehaviour
{
    public Text ResultText;

    [Header("Infos")]
    [SerializeField] private string S3BucketName;
    [Tooltip("FileName with Extesion. E.G file.txt")] [SerializeField] private string fileNameOnBucket;
    [Tooltip("Path and FileName with Extesion. E.G Documents/file.txt")] [SerializeField] private string pathFileUpload;

    [Header("AWS Setup")]
    [SerializeField] private string identityPoolId;
    [SerializeField] private string cognitoIdentityRegion = RegionEndpoint.EUCentral1.SystemName;
    [SerializeField] private string s3Region = RegionEndpoint.EUCentral1.SystemName;

    // Variables privates
    private int timeoutGetObject = 5; // seconds
    private string resultTimeout = "";
    public Action<GetObjectResponse, string> OnResultGetObject;
    private IAmazonS3 s3Client;
    private AWSCredentials credentials;

    private RegionEndpoint CognitoIdentityRegion
    {
        get { return RegionEndpoint.GetBySystemName(cognitoIdentityRegion); }
    }
    private RegionEndpoint S3Region
    {
        get { return RegionEndpoint.GetBySystemName(s3Region); }
    }
    private AWSCredentials Credentials
    {
        get
        {
            if (credentials == null)
                credentials = new CognitoAWSCredentials(identityPoolId, RegionEndpoint.EUCentral1);
            return credentials;
        }
    }
    private IAmazonS3 Client
    {
        get
        {
            if (s3Client == null)
            {
                s3Client = new AmazonS3Client(Credentials, S3Region);
            }
            //test comment
            return s3Client;
        }
    }

    private void Start()
    {
        UnityInitializer.AttachToGameObject(this.gameObject);
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;

    }

    private void UploadObjectForBucket(string pathFile, string S3BucketName, string fileNameOnBucket)
    {
        ResultText.text = "Retrieving the file";
        ResultText.text += "\nCreating request object";
        ResultText.text += "\nMaking HTTP post call";

        UploadObjectForBucket(pathFile, S3BucketName, fileNameOnBucket, (result, error) =>
        {
            if (string.IsNullOrEmpty(error))
            {
                ResultText.text += "\nUpload Success";
            }
            else
            {
                ResultText.text += "\nUpload Failed";
            }
        });
    }

    public void UploadObjectForBucket(string pathFile, string S3BucketName, string fileNameOnBucket, Action<PostObjectResponse, string> result)
    {
        if (!File.Exists(pathFile))
        {
            result?.Invoke(null, "FileNotFoundException: Could not find file " + pathFile);
            return;
        }

        var stream = new FileStream(pathFile, FileMode.Open, FileAccess.Read, FileShare.Read);
        var request = new PostObjectRequest()
        {
            Bucket = S3BucketName,
            Key = fileNameOnBucket,
            InputStream = stream,
            CannedACL = S3CannedACL.Private,
            Region = S3Region
        };

        Client.PostObjectAsync(request, (responseObj) =>
        {
            if (responseObj.Exception == null)
                result?.Invoke(responseObj.Response, "");
            else
                result?.Invoke(null, responseObj.Exception.ToString());
        });
    }

    private void ResultTimeoutGetObjectBucket()
    {
        if (string.IsNullOrEmpty(resultTimeout))
        {
            OnResultGetObject?.Invoke(null, "Timeout GetObject");
        }
    }
}
