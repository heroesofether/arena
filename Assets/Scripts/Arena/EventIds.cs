﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventIds
{
    public const int GameStarted = 0,
                     LoadingScreen = 1,
                     StatsLoaded = 2,
                     StatsInitialized = 3,
                     WeaponChoose = 4,
                     ArenaStarted = 5,
                     UpdateUI = 6,
                     ArenaFinished = 7,
                     Test = 100;
                    
                     
}
