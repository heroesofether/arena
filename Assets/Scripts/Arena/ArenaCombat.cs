using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Archon.SwissArmyLib.Events;
using Random = UnityEngine.Random;
using System.Runtime.InteropServices;

public enum GameState
{
    PlayerTurn,
    EnemieTurn
}

public class ArenaCombat : MonoBehaviour, IEventListener
{
    [DllImport("__Internal")]
    private static extern void ArenaFinish(int winnerID);

    private GameState currentState;
    private ArenaPlayer player1;
    private ArenaPlayer player2;

    public static event Action<float, string, string> onAttackText;
    public static event Action<string, string> onMoveText;
    public static event Action<string> onWinText;
    public static event Action<float, float, float> onUpdateHealth;
    public static event Action<string, string> onUpdateName;
    public static event Action<bool> onNextMove;
    public static event Action<float> onUpdateSliderTime;
    public static event Action<float> onGetDamage;

    private bool isWin;
    private bool isDefending;
    private ArenaPlayer currentArenaPlayer;

    private void InitPlayersStats()
    {
        player1 = new ArenaPlayer(
            Stats.player1Skills[0], 
            Stats.player1Skills[1], 
            Stats.player1Skills[2] * 2,
            Stats.player1Skills[3],
            Stats.player1Name);

        player2 = new ArenaPlayer(
            Stats.player2Skills[0],
            Stats.player2Skills[1], 
            Stats.player2Skills[2] * 2,
            Stats.player2Skills[3],
            Stats.player2Name);

        onUpdateHealth(player1.health, player2.health, player1.agility);
        onUpdateName(player1.name, player2.name);

        Debug.Log("Player1 Damage: " + player1.damage + " Player2 Damage: " + player2.damage);
        Debug.Log("Player1 Armor: " + player1.armor + " Player2 Armor: " + player2.armor);
        Debug.Log("Player1 Health: " + player1.health + " Player2 Health: " + player2.health);
        Debug.Log("Player1 Agility: " + player1.agility + " Player2 Agility: " + player2.agility);

        GlobalEvents.Invoke(EventIds.StatsInitialized);
    }

    private void StartCombat()
    {
        var startState = player1.agility > player2.agility ? currentState = GameState.EnemieTurn : currentState = GameState.PlayerTurn;
        //currentState = GameState.PlayerTurn;
        var currentName = player1.agility > player2.agility ? player1.name : player2.name;
        currentArenaPlayer = player1.agility > player2.agility ? player1 : player2;

        onUpdateSliderTime(currentArenaPlayer.agility);

        NextPlayerMove();

        if (!isWin && currentState == GameState.PlayerTurn)
            onMoveText(currentName, " is your turn to move!");
    }

    public void Attack()
    {
        if (isWin)
            return;

        if (currentState == GameState.PlayerTurn)
        {
            var currentDamage = player1.damage * (1 - (player2.armor / 100));
            player2.health = player2.health - currentDamage;

            onAttackText(currentDamage, player1.name, player2.name);

            onUpdateHealth(player1.health, player2.health, player1.agility);
            
            //GlobalEvents.Invoke(EventIds.UpdateUI);

            onUpdateSliderTime(player1.agility);

            CheckForWin();

            NextPlayerMove();
        }

        else
        {
            var currentDamage = player2.damage * (1 - (player1.armor / 100));
            player1.health = player1.health - currentDamage;

            onAttackText(currentDamage, player2.name, player1.name);

            onUpdateHealth(player1.health, player2.health, player2.agility);
            
            //GlobalEvents.Invoke(EventIds.UpdateUI);

            onUpdateSliderTime(player2.agility);

            CheckForWin();

            NextPlayerMove();
        }
    }

    public void Defence()
    {
        isDefending = true;

        onUpdateHealth(player1.health, player2.health, player1.agility);

        onUpdateSliderTime(player1.agility);

        CheckForWin();

        NextPlayerMove();
    }

    private void NextPlayerMove()
    {
        if (currentState == GameState.PlayerTurn)
        {
            currentState = GameState.EnemieTurn;
            onNextMove(false);
            StartCoroutine(EnemyAttackAI());
        }
            
        else 
        {
            currentState = GameState.PlayerTurn;
            onNextMove(true);
            isDefending = false;
        }
    }

    private IEnumerator EnemyAttackAI()
    {   
        if (isWin)
            yield break;

        if (!isWin)
            onMoveText(player2.name, " is thinking...");

        var rand = Random.Range(1, 3.5f);

        yield return new WaitForSeconds(rand);

        var armor = isDefending ? player1.armor * 2 : player1.armor;

        var currentDamage = player2.damage * (1 - (armor / 100));
        player1.health = player1.health - currentDamage;

        onAttackText(currentDamage, player2.name, player1.name);

        onUpdateHealth(player1.health, player2.health, player1.agility);
        
        //GlobalEvents.Invoke(EventIds.UpdateUI);

        onUpdateSliderTime(player2.agility);

        CheckForWin();

        NextPlayerMove();

        if (!isWin)
            onMoveText(player1.name, " is your turn to move!");
    }

    private void CheckForWin()
    {
        if (player1.health <= 0)
        {
            isWin = true;
            onWinText(player2.name);
            GlobalEvents.Invoke(EventIds.ArenaFinished);
#if !UNITY_EDITOR
            ArenaFinish(Stats.player2ID);
#endif
        }

        else if (player2.health <= 0)
        {
            isWin = true;
            onWinText(player1.name);
            GlobalEvents.Invoke(EventIds.ArenaFinished);
#if !UNITY_EDITOR
            ArenaFinish(Stats.player1ID);
#endif
        }
    }

    private void WhosCurrentPlayer(float playerAgility)
    {
        playerAgility = currentArenaPlayer.agility;
    }

    private void OnEnable()
    {
        UIArenaManager.onTimeOver += NextPlayerMove;

        GlobalEvents.AddListener(EventIds.StatsLoaded, this);
        GlobalEvents.AddListener(EventIds.WeaponChoose, this);
    }

    private void OnDisable()
    {
        UIArenaManager.onTimeOver -= NextPlayerMove;

        GlobalEvents.RemoveListener(EventIds.StatsLoaded, this);
        GlobalEvents.RemoveListener(EventIds.WeaponChoose, this);
    }

    public void OnEvent(int eventId)
    {
        switch (eventId)
        {
            case EventIds.StatsLoaded:
                InitPlayersStats();
                break;
            case EventIds.WeaponChoose:
                StartCombat();
                break;
        }
    }
}

public class ArenaPlayer
{
    public float armor { get; set; }
    public float damage { get; set; }
    private float _health;
    public float health
    {
        get {  
                if (_health > 0)
                    return _health;
                else return 0;
            }

        set { _health = value; }
    }
    public float agility { get; set; }
    public string name { get; set; }

    public ArenaPlayer(float damage, float armor, float health, float agility, string name)
    {
        this.armor = armor;
        this.damage = damage;
        this.health = health;
        this.agility = agility;
        this.name = name;
    }
}
