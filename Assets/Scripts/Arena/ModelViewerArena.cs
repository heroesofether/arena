﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ModelViewerArena : MonoBehaviour
{
    private float rotateSpeed = 20;
    private float rotateSpeedModifier = 1000;
    private Vector3 mPrevPos = Vector3.zero;
    private Vector3 mPosDetla = Vector3.zero;
    private Camera mCamera;

    private void Start()
    {
        mCamera = Camera.main.GetComponent<Camera>();
    }

    private void Update()
    {
        Rotator360();
        //RotateWithMouse();
    }

    private void Rotator360()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        var mouse = Camera.main.ScreenPointToRay(Input.mousePosition);
        var forward = transform.TransformDirection(Vector3.forward);

        RaycastHit hit;
        GameObject player = null;

        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(mouse, out hit, Mathf.Infinity))
            {   
                mPosDetla = Input.mousePosition - mPrevPos;
                player = hit.collider.gameObject;
            }

            if (player == null)
                return;

            if (Vector3.Dot(player.transform.up, Vector3.up) >= 0)
                player.transform.Rotate(player.transform.up, -Vector3.Dot(mPosDetla, Camera.main.transform.right) * rotateSpeed * Time.deltaTime, Space.World);

            else
                player.transform.Rotate(player.transform.up, Vector3.Dot(mPosDetla, Camera.main.transform.right) * rotateSpeed * Time.deltaTime, Space.World);

        }

        mPrevPos = Input.mousePosition;
    }

    
    private void RotateWithMouse()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

            var mouse = Camera.main.ScreenPointToRay(Input.mousePosition);
            var forward = transform.TransformDirection(Vector3.forward);

        if (Input.GetMouseButton(0))
        {
            
            mPosDetla = mouse.origin - mPrevPos;

            RaycastHit hit;
            GameObject player;

            if (Physics.Raycast(mouse, out hit, Mathf.Infinity))
            {
                //Debug.DrawRay(mouse, forward * hit.distance, Color.blue);

                player = hit.collider.gameObject;

                Debug.Log("Touched it " + mouse.origin);

                var rotationY = Quaternion.Euler(0f, -mouse.origin.x * rotateSpeedModifier, 0f);

                //player.transform.rotation = rotationY * transform.rotation;

                if (Vector3.Distance(mPrevPos, mouse.origin) > 0.001f)
                    player.transform.rotation = Quaternion.Slerp(player.transform.rotation, rotationY, Time.deltaTime * rotateSpeedModifier);
            }
        }

        mPrevPos = mouse.origin;
    }
}

