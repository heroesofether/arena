﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Archon.SwissArmyLib;
using Archon.SwissArmyLib.Events;

public class Arena : MonoBehaviour, IEventListener
{
    [Header("For Debug")]
    [SerializeField] private string playerTokenIDDebug;
    [SerializeField] private string enemyTokenIDDebug;

    private GenomeManager gManager;
    private GameObject player1;
    private GameObject player2;

    private GameObject weapon;
    private GameObject clubWeapon;
    private GameObject spearWeapon;

    private StatsLoader statsLoader;

    private void Awake() 
    {
       gManager = GetComponent<GenomeManager>(); 
       statsLoader = GetComponent<StatsLoader>();
    }

    private void Start() 
    {
#if UNITY_EDITOR
        //StartArena(12, 13); // call for old start arena
        StartArenaHelper(playerTokenIDDebug + "|" + enemyTokenIDDebug); // call for start arena
#endif
    }

    public void StartArenaHelper(string tokenIds)
    {
        string[] tokenId = tokenIds.Split('|');
        var x = int.Parse(tokenId[0]);
        var y = int.Parse(tokenId[1]);
        
        StartArena(x, y);
    }

    public void StartArena(int tokenID1, int tokenID2)
    {
        StartCoroutine(statsLoader.LoadStats(tokenID1));
        StartCoroutine(statsLoader.LoadStats2(tokenID2));

        GlobalEvents.Invoke(EventIds.GameStarted);
    }

    private void LoadPlayer()
    {
        var x = statsLoader.jsonPlayer1.data.genes;
        var y = statsLoader.jsonPlayer2.data.genes;

        Init(x, y);
    }

    private void Init(string playerTokenID, string enemyTokenID)
    {
        var collSize = new Vector3(0.25f, 1, 0.25f);
        
        var genomeList =  GenomeHelper.GeneratorWithSubstring(playerTokenID);
        player1 = gManager.Generate(genomeList);
        var boxColl1 = player1.AddComponent<BoxCollider>();
        boxColl1.size = collSize;
        var worldPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 7f, Screen.height / 2, 5));
        player1.transform.position = worldPosition;
        player1.transform.rotation = Quaternion.Euler(0, 65, 0);
        player1.SetActive(false);

        genomeList = GenomeHelper.GeneratorWithSubstring(enemyTokenID);
        player2 = gManager.Generate(genomeList);
        var boxColl2 = player2.AddComponent<BoxCollider>();
        boxColl2.size = collSize;
        worldPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 1.2f, Screen.height / 1.9f, 6));
        player2.transform.position = worldPosition;
        player2.transform.rotation = Quaternion.Euler(0, -140, 0);
        player2.SetActive(false);

        //GlobalEvents.Invoke(EventIds.ArenaStarted);
    }

    public void ChooseWeapon()
    {
        player1.SetActive(true);
        GameObject g = GameObject.Find("Weapon Holder");
        player1.SetActive(false);

        if (weapon)
        {
            //Instantiate(weapon, g.transform);
            //GlobalEvents.Invoke(EventIds.WeaponChoose);
        }
    }
    
    public void ChooseClub()
    {
        weapon = clubWeapon;
        ChooseWeapon();
    }

    public void ChooseSpear()
    {
        weapon = spearWeapon;
        ChooseWeapon();
    }

    private void OnEnable()
    {
        GlobalEvents.AddListener(EventIds.LoadingScreen, this);
        GlobalEvents.AddListener(EventIds.StatsLoaded, this);
        GlobalEvents.AddListener(EventIds.WeaponChoose, this);
        GlobalEvents.AddListener(EventIds.ArenaFinished, this);
    }

    private void OnDisable()
    {
        GlobalEvents.RemoveListener(EventIds.LoadingScreen, this);
        GlobalEvents.RemoveListener(EventIds.StatsLoaded, this);
        GlobalEvents.RemoveListener(EventIds.WeaponChoose, this);
        GlobalEvents.RemoveListener(EventIds.ArenaFinished, this);
    }

    public void OnEvent(int eventId)
    {
        switch (eventId)
        {
            case EventIds.LoadingScreen:
                //ChooseWeapon();
                break;
            case EventIds.StatsLoaded:
                LoadPlayer();
                break;
            case EventIds.WeaponChoose:
                player1.SetActive(true);
                player2.SetActive(true);
                break;   
        }
    }
}
