﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pixelplacement;

public class UIFloatText : MonoBehaviour
{
    [SerializeField] private Color[] colors;
    private Text text;
    private float duration = 1;

    private void Awake() 
    {
        text = GetComponent<Text>();
    }

    public void Init(float damage)
    { 
        var randX = Random.Range(-100f, 100f);
        var randY = Random.Range(20f, 240f);

        text.text = damage.ToString();
        text.color = colors[Random.Range(0, colors.Length)];

        Tween.Position(transform, transform.position + new Vector3(randX, randY), duration + 1, 0);
        //Tween.Shake(transform, _cameraInitialPosition, Vector3.one * .25f, 1f, 0);
        Tween.LocalScale(transform, new Vector3(0.5f, 0.5f, 0.5f), duration, 0);
        Tween.Color(text, Color.clear, duration + .5f, 0);
        
        //Tween.Shake(transform, Vector3.zero, Vector3.one, 1, 0);
        Destroy(gameObject, 1f);
    }
}
