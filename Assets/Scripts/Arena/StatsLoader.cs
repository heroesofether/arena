﻿using System.Collections;
using System.Collections.Generic;
using Archon.SwissArmyLib.Events;
using UnityEngine;
using UnityEngine.Networking;

public class StatsLoader : MonoBehaviour
{
    [HideInInspector] public Hero jsonPlayer1;
    [HideInInspector] public Hero jsonPlayer2;

    private void Start()
    {
        //StartArena(13);
    }

    public void StartArena(int tokenID)
    {
       //StartCoroutine(LoadStats(tokenID));
    }

    public IEnumerator LoadStats(int tokenID)
    {
        var url = "https://api.heroesofether.io/api/contract/getHero/" + tokenID;
        var request = UnityWebRequest.Get(url);

        if (request.isNetworkError || request.isHttpError)
            yield break;

        yield return request.SendWebRequest();
        var data = request.downloadHandler.text;

        var json = JsonSerializer.JsonRead(data);

        if (!json.success)
            yield break;

        //do some
        this.jsonPlayer1 = json;
        Stats.player1ID = tokenID;

        for (int i = 0; i < json.data.skills.Length; i++)
        {
            Stats.player1Skills[i] = json.data.skills[i] * 10;
        }

        Stats.player1Name = json.data.api.username;
    }

    public IEnumerator LoadStats2(int tokenID)
    {
        var url = "https://api.heroesofether.io/api/contract/getHero/" + tokenID;
        var request = UnityWebRequest.Get(url);

        if (request.isNetworkError || request.isHttpError)
            yield break;

        yield return request.SendWebRequest();
        var data = request.downloadHandler.text;

        var json = JsonSerializer.JsonRead(data);

        if (!json.success)
            yield break;

        //do some
        this.jsonPlayer2 = json;
        Stats.player2ID = tokenID;

        for (int i = 0; i < json.data.skills.Length; i++)
        {
            Stats.player2Skills[i] = json.data.skills[i] * 10;
        }

        Stats.player2Name = json.data.api.username;

        yield return new WaitForSeconds(0.4f);

        GlobalEvents.Invoke(EventIds.StatsLoaded);
    }
}

struct Stats
{
    private static float[] _player1Skills = new float[9];
    public static float[] player1Skills
    {
        get { return _player1Skills; }
        set { _player1Skills = value; }
    } 

    private static float[] _player2Skills = new float[9];
    public static float[] player2Skills
    {
        get { return _player2Skills; }
        set { _player2Skills = value; }
    }

    public static string player1Name {get; set;}
    public static string player2Name {get; set;}

    public static int player1ID { get; set; }
    public static int player2ID { get; set; }
}