﻿using System;
using Archon.SwissArmyLib.Events;
using Pixelplacement;
using Pixelplacement.TweenSystem;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class UIArenaManager : MonoBehaviour, IEventListener
{
    public GameObject startscreenTop;
    public GameObject startscreenBot;
    public GameObject startscreenTopTarget;
    public GameObject startscreenBotTarget;

    #region UI
    public GameObject logScroll;

    public Button[] playButtons; // 0 - attack, 1 - counter, 2 - grapple, 3 - punch
    
    public GameObject readyScreen;
    public Text chooseWeaponText;
    #endregion

    public AnimationCurve curve;
    public AnimationCurve curveBack;
    public Tween.LoopType loop;

    public GameObject content;
    public GameObject prefabText;

    [SerializeField] private GameObject[] floatTextHandler;
    [SerializeField] private GameObject floatText;

    public GameObject panelName1;
    public GameObject panelName2;

    public Text player1TextHP;
    public Text player2TextHP;

    public Text player1NameText;
    public Text player2NameText;

    private Vector3 topPos;
    private Vector3 botPos;

    private Vector3 targetTop;
    private Vector3 targetBot;

    public Slider timeSlider;

    private bool isStartSlider;

    public static event Action onTimeOver;

    private int stepCount;
    private float playerSliderTime;

    private void Start()
    {
        //StartCoroutine(LoadText());

        var r1 = startscreenTop.GetComponent<RectTransform>();
        //r1.sizeDelta = new Vector2(Screen.width, Screen.height / 2);

        var r2 = startscreenBot.GetComponent<RectTransform>();
        //r2.sizeDelta = new Vector2(Screen.width, Screen.height / 2);

        targetTop = new Vector2(Screen.width / 2, startscreenTop.transform.position.y);
        targetBot = new Vector2(Screen.width / 2, startscreenBot.transform.position.y);

        topPos = startscreenTop.transform.position;
        botPos = startscreenBot.transform.position;

        logScroll.SetActive(false);

        foreach (var button in playButtons)
        {
            button.gameObject.SetActive(false);
        }

        panelName1.SetActive(false);
        panelName2.SetActive(false);

        timeSlider.gameObject.SetActive(false);

        Transform text1 = startscreenTop.transform.GetChild(0);
        Transform text2 = startscreenBot.transform.GetChild(0);

        Tween.LocalScale(text1, new Vector3(1.2f, 1.2f, 1.2f), 1.2f, 0, curve, Tween.LoopType.PingPong);
        Tween.LocalScale(text2, new Vector3(1.2f, 1.2f, 1.2f), 1.2f, 0, curve, Tween.LoopType.PingPong);

        Tween.LocalScale(chooseWeaponText.transform, new Vector3(1.2f, 1.2f, 1.2f), 1.2f, 0, curve, Tween.LoopType.PingPong);
    }

    private void Update() 
    {
        if (isStartSlider)
        {
            timeSlider.value = timeSlider.value - 0.05f * Time.deltaTime;

            if (timeSlider.value <= 0)
            {
                onTimeOver();
                timeSlider.value = playerSliderTime / 100f;
            } 
        }
    }

    private void CloseLoadingScreen()
    {
        Tween.Position(startscreenTop.transform, topPos, targetTop, 0.7f, 0.2f, curve, loop);
        Tween.Position(startscreenBot.transform, botPos, targetBot, 0.7f, 0.2f, curve, loop);
    }

    private void OpenLoadingScreen()
    {
        Tween.Position(startscreenTop.transform, startscreenTop.transform.position, topPos, 1.4f, 2.2f, curveBack, loop, OnLoad);
        Tween.Position(startscreenBot.transform, startscreenBot.transform.position, botPos, 1.4f, 2.2f, curveBack, loop);

        //OnUpdateUI();
        readyScreen.SetActive(true);
    }

    private void OnLoad()
    {
        GlobalEvents.Invoke(EventIds.LoadingScreen);
    }

    private void OnUpdateUI(float player1Health, float player2Health, float time)
    {
        player1TextHP.text = "HP: " + player1Health;
        player2TextHP.text = "HP: " + player2Health;

        timeSlider.value = time / 100;
    }

    public void OnReadyScreenStart()
    {        
        foreach (var button in playButtons)
        {
            //button.gameObject.SetActive(true);
        }

        playButtons[0].gameObject.SetActive(true);
        playButtons[1].gameObject.SetActive(true);
        logScroll.SetActive(true);

        panelName1.SetActive(true);
        panelName2.SetActive(true);

        timeSlider.gameObject.SetActive(true);

        readyScreen.SetActive(false);

        isStartSlider = true;

        GlobalEvents.Invoke(EventIds.WeaponChoose);
    }

    private void InitHitDamageText(float damage)
    {
        GameObject g = Instantiate(floatText, floatTextHandler[0].transform);
        var floatTxt = g.GetComponent<UIFloatText>();
        floatTxt.Init(damage);
    }

    private void AddDamageText(float damage, string playerName, string enemyName)
    {
        string[] words = {"hit", "smashed", "attacked", "punched"};

        stepCount++;

        GameObject g = Instantiate(prefabText, content.transform);
        var t = g.GetComponent<Text>();
        t.resizeTextForBestFit = true;
        t.text = "Step " + stepCount + ": " + playerName + " " + words[Random.Range(0, words.Length)] + " " + enemyName  + " on " + damage + " damage! "; 
        t.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        //compo.transform.parent = g.transform;
        content.transform.SetParent(g.transform);
    }

    private void AddMoveText(string playerName, string moveText)
    {
        GameObject g = Instantiate(prefabText, content.transform);
        var t = g.GetComponent<Text>();
        t.resizeTextForBestFit = true;
        t.text = playerName + moveText; 
        t.color = Color.black;
        t.alignment = TextAnchor.MiddleCenter;
        content.transform.SetParent(g.transform);
    }

    private void AddWinText(string winner)
    {
        GameObject g = Instantiate(prefabText, content.transform);
        var t = g.GetComponent<Text>();
        t.resizeTextForBestFit = true;
        t.text = "Battle is finished! " + winner + " won!";
        t.alignment = TextAnchor.MiddleCenter;
    }

    private void DisableUI()
    {
        timeSlider.value = 0;
        timeSlider.gameObject.SetActive(false);

        //attackButton.interactable = false;
        //defendButton.interactable = false;

        foreach (var button in playButtons)
        {
            button.gameObject.SetActive(false);
        }
    }

    private void DisableButton(bool b)
    {
        foreach (var button in playButtons)
        {
            button.interactable = b;
        }
    }

    private void UpdateName(string player1, string player2)
    {
        player1NameText.text = player1;
        player2NameText.text = player2;
    }

    private void UpdateSlidetTime(float time)
    {
        playerSliderTime = time;
    }

    private void OnEnable()
    {
        ArenaCombat.onAttackText += AddDamageText;
        ArenaCombat.onMoveText += AddMoveText;
        ArenaCombat.onWinText += AddWinText;
        ArenaCombat.onUpdateHealth += OnUpdateUI;
        ArenaCombat.onNextMove += DisableButton;
        ArenaCombat.onUpdateName += UpdateName;
        ArenaCombat.onUpdateSliderTime += UpdateSlidetTime;

        GlobalEvents.AddListener(EventIds.GameStarted, this);
        GlobalEvents.AddListener(EventIds.ArenaFinished, this);
        GlobalEvents.AddListener(EventIds.StatsInitialized, this);
    }

    private void OnDisable()
    {
        ArenaCombat.onAttackText -= AddDamageText;
        ArenaCombat.onMoveText -= AddMoveText;
        ArenaCombat.onWinText -= AddWinText;
        ArenaCombat.onUpdateHealth -= OnUpdateUI;
        ArenaCombat.onNextMove -= DisableButton;
        ArenaCombat.onUpdateName -= UpdateName;
        ArenaCombat.onUpdateSliderTime -= UpdateSlidetTime;

        GlobalEvents.RemoveListener(EventIds.GameStarted, this);
        GlobalEvents.RemoveListener(EventIds.ArenaFinished, this);
        GlobalEvents.RemoveListener(EventIds.StatsInitialized, this);
    }

    public void OnEvent(int eventId)
    {
        switch (eventId)
        {
            case EventIds.GameStarted:
                //CloseLoadingScreen();
                break;
            case EventIds.StatsInitialized:
                readyScreen.SetActive(true);
                //weaponPicker.SetActive(true);
                break;
            case EventIds.WeaponChoose:
                //OnReadyScreenStart();
                break;
            case EventIds.ArenaFinished:
                DisableUI();
                isStartSlider = false;
                break;
        }
    }
}


public class UIHelper
{
    public static void ButtonCooldown(Button button)
    {

    }
}
