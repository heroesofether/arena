﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System;
using System.Linq;

public class WebRequest : MonoBehaviour
{
    private ScreenshotTransparent screenshotT;

    private const string urlGet = "https://api.heroesofether.io/api/contract/getHero/";
    private const string urlPost = "https://api.heroesofether.io/api/heroes/upload-image";

    public static event Action onLoadedScreenshot;

    public IEnumerator OnResponceGenerator(int tokenID, Action<string> Generator)
    {
        var url = urlGet + tokenID;
        // Using the static constructor
        var request = UnityWebRequest.Get(url);

        // Wait for the response and then get our data
        yield return request.SendWebRequest();
        var data = request.downloadHandler.text;
        //debug.text = data;
        Debug.Log("Data" + data);

        var json = JsonSerializer.JsonRead(data);
        Generator(json.data.genes);
        
        screenshotT = Camera.main.GetComponent<ScreenshotTransparent>();
        screenshotT.MakeScreenshot();
        yield return new WaitForSeconds(0.5f);
        var output64 = screenshotT.base64output;
        Debug.Log("Base64 is: " + output64);
#if !UNITY_EDITOR
        //startCoroutine();
        StartCoroutine(OnPostScreenshot(tokenID, output64, onLoadedScreenshot));
#endif
    }

    public IEnumerator OnResponceViewer(int tokenID, Action<string> act, Action<bool> callback)
    {
        var url = urlGet + tokenID;
        var request = UnityWebRequest.Get(url);

        if (request.isNetworkError || request.isHttpError)
            yield break;

        yield return request.SendWebRequest();
        var data = request.downloadHandler.text;

        var json = JsonSerializer.JsonRead(data);

        if (!json.success)
            yield break;

        //gManager.GeneratorWithSubstring(json.data.genes);
        act(json.data.genes);
        callback(true);
    }

    public IEnumerator OnPostScreenshot(int tokenID, string output64, Action callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("token", tokenID);
        form.AddField("image", output64);

        using (UnityWebRequest www = UnityWebRequest.Post(urlPost, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }

            else
            {
                callback();
                Debug.Log("Form upload complete!");
            }
        }
    }
}
