﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using System.Runtime.InteropServices;

public class GenomeManager : MonoBehaviour
{
    #region React
    [DllImport("__Internal")]
    private static extern void GameStart();
    [DllImport("__Internal")]
    private static extern void GameOver();
    #endregion

    #region Varibles
    [Header("For Debug")]
    [SerializeField] private bool turnOnGenerator;
    [SerializeField] private int debugTokenID;
    [SerializeField] private string debugGenomeString;
    [SerializeField] private GameObject justasPrefab;
    #endregion
    
    private bool isBoy;
    private GameObject _currentPlayer = null;
    [SerializeField] private GameObject boyPrefab, girlPrefab;
    [SerializeField] private MeshRenderer[] playerMeshRenderer;
    [SerializeField] private SkinnedMeshRenderer[] playerSkinnedMeshRenderer;
    [SerializeField] private Material boyBodyMat;
    [SerializeField] private Material girlBodyMat;
    [SerializeField] private ResourcesSkinColors[] skinColor;
    [SerializeField] private Resources3D[] boyEyes, boyBeard, boyEyebrows;
    [SerializeField] private Resources3D[] girlEyes, girlEyebrows;
    [SerializeField] private ResourcesClothes boyClothes;
    [SerializeField] private ResourcesClothes girlClothes;
    [SerializeField] private ResourcesHairs[] boyHairs;
    [SerializeField] private ResourcesHairs[] boyBeards;
    [SerializeField] private ResourcesHairs[] girlHairs;
    [SerializeField] private ResourcesHairsColors[] hairColor;

    private WebRequest wRequest;

    #region Encapsulation
    public GameObject currentPlayer
    {
        get { return _currentPlayer; }
    }
    #endregion

    private void Awake()
    {
        wRequest = GetComponent<WebRequest>();
    }

    private void Start()
    {
        OnStart();
        
        if (turnOnGenerator)
        {
            Submit(debugTokenID); // call to start debug generator/screenshot
            //SubmitGenome();
        }
    }

    public void Submit(int indexID)
    {
        var onGenRespon = wRequest.OnResponceGenerator(indexID, GeneratorWithSubstring);
        StartCoroutine(onGenRespon);
    }

    public void GeneratorWithSubstring(string input)
    {
        List<int> genomeList = new List<int>();

        int stringToInt(string s, int startIndex, int length)
        {
            return Convert.ToInt32(s.Substring(startIndex, length));
        }

        if (genomeList.Any())
            genomeList.Clear();

        genomeList.Add(stringToInt(input, 0, 1)); // 0 body
        genomeList.Add(stringToInt(input, 1, 2)); // 1 eye color
        genomeList.Add(stringToInt(input, 3, 1)); // 2 skin color
        genomeList.Add(stringToInt(input, 4, 2)); // 3 hair color
        genomeList.Add(stringToInt(input, 6, 2)); // 4 faceHair color

        genomeList.Add(stringToInt(input, 8, 1)); // 5 hair
        genomeList.Add(stringToInt(input, 9, 1)); // 6 eyebrow
        genomeList.Add(stringToInt(input, 10, 1)); // 7 eye
        genomeList.Add(stringToInt(input, 11, 1)); // 8 ear
        genomeList.Add(stringToInt(input, 12, 1)); // 9 nose 
        genomeList.Add(stringToInt(input, 13, 1)); // 10 mouth
        genomeList.Add(stringToInt(input, 14, 1)); // 11 jaw

        Generate(genomeList);
    }
    
    public GameObject Generate(List<int> list)
    {
        var currentHairColor = Color.white;
        var currentFaceHairColor = Color.white;
        
        List<int> justasList = new List<int>()
        {
            7, 23, 1, 52, 52, 1, 2, 3, 2, 4, 2, 1
        };

        var isJustas = list.SequenceEqual(justasList) ? true : false;

        if (isJustas)
        {
            GameObject j = Instantiate(justasPrefab, new Vector3(0, 1f, -8.95f), transform.rotation * Quaternion.Euler(0, 180f, 0));
            _currentPlayer = j;
            return j;
        }

        float tempFloat;
        Debug.Log("List count is " + list.Count);

        int GetIndex(int ind, int[] arr)
        {
            foreach (var index in arr)
            {
                if (ind == index)
                    return index;
            }

            return 0;
        }

        Debug.Log("Genome is: " + "Body - " + list[0] + ", Eye color - " + list[1] + ", Skin color - " + list[2] 
            + ", Hair color - " + list[3] + ", Facehair color - " + list[4] + ", Hair - " + list[5] + ", Eyebrow - " + list[6]
            + ", Eye - " + list[7] + ", Ear - " + list[8] + ", Nose - " + list[9] + ", Mouth - " + list[10] + ", Jaw - " + list[11]);

        for (int i = 0; i < list.Count; i++)
        {
            var value = list[i];

            #region BODY
            //body
            if (i == 0)
            {
                _currentPlayer = SetGender(value);

                //tempFloat = Mathf.Clamp(value, 1f, 6f);
                switch (value)
                {
                    case 1:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(0, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(1, 0);
                        break;
                    case 2:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(0, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(1, 0);
                        break;
                    case 3:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(0, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(1, 0);
                        break;
                    case 4:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(0, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(1, 0);
                        break;
                    case 5:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(0, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(1, 100);
                        break;
                    case 6:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(0, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(1, 100);
                        break;
                    default:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(0, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(1, 0);
                        break;
                }   

                var thin = playerSkinnedMeshRenderer[1].GetBlendShapeWeight(0);
                //playerSkinnedMeshRenderer[2].SetBlendShapeWeight(0, thin);
                var musc = playerSkinnedMeshRenderer[1].GetBlendShapeWeight(1);
                //playerSkinnedMeshRenderer[2].SetBlendShapeWeight(1, musc);

                SetClothes(currentPlayer, thin, musc);
            }
            #endregion

            #region COLORS
            //eye color
            if (i == 1)
            {
                if (isBoy)
                { 
                    for (int j = 0; j < boyEyes.Length; j++)
                    {
                        if (value == GetIndex(value, boyEyes[j].indx))
                        {
                            playerMeshRenderer[1].material = boyEyes[j].mat;
                            break;
                        }

                        else playerMeshRenderer[1].material = boyEyes[0].mat;
                    }
                }
                    
                else
                {
                    for (int j = 0; j < girlEyes.Length; j++)
                    {
                        if (value == GetIndex(value, girlEyes[j].indx))
                        {
                            playerMeshRenderer[1].material = girlEyes[j].mat;
                            break;
                        }

                        else playerMeshRenderer[1].material = girlEyes[0].mat;
                    }
                }
            }
            
            //body color
            if (i == 2)
            {   
                if (isBoy)
                { 
                    for (int j = 0; j < skinColor.Length; j++)
                    {
                        if (value == GetIndex(value, skinColor[j].indx))
                        {
                            playerSkinnedMeshRenderer[1].material = boyBodyMat;
                            playerSkinnedMeshRenderer[1].material.color = skinColor[value].skinColor;
                            break;
                        }

                        else 
                            playerSkinnedMeshRenderer[1].material = boyBodyMat;
                    }
                }
                    
                else
                {
                    for (int j = 0; j < skinColor.Length; j++)
                    {
                        if (value == GetIndex(value, skinColor[j].indx))
                        {
                            playerSkinnedMeshRenderer[1].material = girlBodyMat;
                            playerSkinnedMeshRenderer[1].material.color = skinColor[value].skinColor;
                            break;
                        }

                        else 
                            playerSkinnedMeshRenderer[1].material = girlBodyMat;
                    }
                }
            }

            //hair color
            if (i == 3)
            {
                if (isBoy)
                { 
                    for (int j = 0; j < hairColor.Length; j++)
                    {
                        if (value == GetIndex(value, hairColor[j].indx))
                        {
                            currentHairColor = hairColor[j].hairColor;
                            break;
                        }

                        else
                             currentHairColor = hairColor[0].hairColor;
                        
                    }
                }
                    
                else
                {
                    for (int j = 0; j < hairColor.Length; j++)
                    {
                        if (value == GetIndex(value, hairColor[j].indx))
                        {
                            currentHairColor = hairColor[j].hairColor;
                            break;
                        }

                        else 
                            currentHairColor = hairColor[0].hairColor;
                    }
                }
            }

            //faceHair color
            if (i == 4)
            {
                if (isBoy)
                { 
                    for (int j = 0; j < hairColor.Length; j++)
                    {
                        if (value == GetIndex(value, hairColor[j].indx))
                        {
                            currentFaceHairColor = hairColor[j].hairColor;
                            playerMeshRenderer[2].material = boyBeard[0].mat;
                            playerMeshRenderer[2].material.color = currentFaceHairColor;
                            playerSkinnedMeshRenderer[0].material = boyEyebrows[0].mat;
                            playerSkinnedMeshRenderer[0].material.color = currentFaceHairColor;
                            break;
                        }

                        else 
                        {
                            currentFaceHairColor = hairColor[0].hairColor;
                            playerMeshRenderer[2].material = boyBeard[0].mat;
                            playerMeshRenderer[2].material.color = currentFaceHairColor;
                            playerSkinnedMeshRenderer[0].material = boyEyebrows[0].mat;
                            playerSkinnedMeshRenderer[0].material.color = currentFaceHairColor;
                        } 
                    }
                }
                    
                else
                {
                    for (int j = 0; j < hairColor.Length; j++)
                    {
                        if (value == GetIndex(value, hairColor[j].indx))
                        {
                            currentFaceHairColor = hairColor[j].hairColor;
                            playerSkinnedMeshRenderer[0].material = girlEyebrows[0].mat;
                            playerSkinnedMeshRenderer[0].material.color = currentFaceHairColor;
                            break;
                        }

                        else
                        {
                            currentFaceHairColor = hairColor[0].hairColor;
                            playerSkinnedMeshRenderer[0].material = girlEyebrows[0].mat;
                            playerSkinnedMeshRenderer[0].material.color = currentFaceHairColor;
                        } 
                    }
                }
            }
            #endregion

            #region FACE
            //hair
            if (i == 5)
            {   
                if (isBoy)
                {
                    for (int j = 0; j < boyHairs.Length; j++)
                    {
                        if (value == GetIndex(value, boyHairs[j].indx))
                        {
                            GameObject hair = Instantiate(boyHairs[value].hair, currentPlayer.transform.GetChild(0));
                            var mesh = hair.GetComponent<MeshRenderer>();
                            mesh.material.color = currentHairColor;
                            break;

                            /* if (value == 7)
                            {
                                hair = Instantiate(boyBeards[0].hair, currentPlayer.transform.GetChild(0));
                                mesh = hair.GetComponent<MeshRenderer>();
                                mesh.material.color = currentColor;
                            }

                            if (value == 8)
                            {
                                hair = Instantiate(boyBeards[1].hair, currentPlayer.transform.GetChild(0));
                                mesh = hair.GetComponent<MeshRenderer>();
                                mesh.material.color = currentColor;
                            }

                            if (value == 9)
                            {
                                hair = Instantiate(boyBeards[2].hair, currentPlayer.transform.GetChild(0));
                                mesh = hair.GetComponent<MeshRenderer>();
                                mesh.material.color = currentColor;
                            }

                            if (value == 10)
                            {
                                hair = Instantiate(boyBeards[3].hair, currentPlayer.transform.GetChild(0));
                                mesh = hair.GetComponent<MeshRenderer>();
                                mesh.material.color = currentColor;
                            } */
                        }


                    }
                }

                else 
                {
                    for (int j = 0; j < girlHairs.Length; j++)
                    {
                        if (value == GetIndex(value, girlHairs[j].indx))
                        {
                            GameObject hair = Instantiate(girlHairs[value].hair, currentPlayer.transform.GetChild(0));
                            var mesh = hair.GetComponent<MeshRenderer>();
                            mesh.material.color = currentHairColor;
                            break;
                        }
                    }
                }
            }

            //eyebrows
            if (i == 6)
            {
                tempFloat = Mathf.Clamp(list[i], 0f, 2f);
                switch (tempFloat)
                {
                    case 0:
                        playerSkinnedMeshRenderer[0].SetBlendShapeWeight(0, 0);
                        break;
                    case 1:
                        playerSkinnedMeshRenderer[0].SetBlendShapeWeight(0, 100);
                        break;
                    case 2:
                        playerSkinnedMeshRenderer[0].SetBlendShapeWeight(1, 100);
                        break;
                    /* case 3:
                        playerSkinnedMeshRenderer[0].SetBlendShapeWeight(0, 100);
                        playerSkinnedMeshRenderer[0].SetBlendShapeWeight(1, 100);
                        break; */
                    default:
                        playerSkinnedMeshRenderer[0].SetBlendShapeWeight(0, 0);
                        break;
                }
            }

            //eyes
            if (i == 7)
            {
                tempFloat = Mathf.Clamp(list[i], 0f, 2f);
                switch (tempFloat)
                {
                    case 0:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(8, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(9, 0);
                        break;
                    case 1:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(8, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(9, 0);
                        break;
                    case 2:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(8, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(9, 100);
                        break;
                    /* case 3:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(8, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(9, 100);
                        break; */
                    default:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(8, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(9, 0);
                        break;
                }
            }

            //ears
            if (i == 8)
            { 
                continue;
            }

            //nose
            if (i == 9)
            {
                tempFloat = Mathf.Clamp(list[i], 0f, 7f);
                switch (tempFloat)
                {
                    case 0:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 0);
                        break;
                    case 1:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 0);
                        break;
                    case 2:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 0);
                        break;
                    case 3:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 100);
                        break;
                    case 4:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 0);
                        break;
                    case 5:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 100);
                        break;
                    case 6:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 100);
                        break;
                    case 7:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 100);
                        break;
                    default:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(2, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(3, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(4, 0);
                        break;
                }
            }

            //mouth (lips)
            if (i == 10)
            {
                tempFloat = Mathf.Clamp(list[i], 0f, 7f);
                switch (tempFloat)
                {
                    case 0:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 0);
                        break;
                    case 1:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 0);
                        break;
                    case 2:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 0);
                        break;
                    case 3:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 100);
                        break;
                    case 4:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 0);
                        break;
                    case 5:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 100);
                        break;
                    case 6:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 100);
                        break;
                    case 7:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 100);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 100);
                        break;
                    default:
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(5, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(6, 0);
                        playerSkinnedMeshRenderer[1].SetBlendShapeWeight(7, 0);
                        break;
                }
            }

            //jaw
            if (i == 11)
            {
                // not impleneted in model yet
            }

            #endregion

        }

        if (currentPlayer.transform.GetChild(0).Find("Hair"))
            currentPlayer.transform.GetChild(0).Find("Hair").gameObject.SetActive(false);

        /* if (currentPlayer.transform.GetChild(0).Find("Beard"))
            currentPlayer.transform.GetChild(0).Find("Beard").gameObject.SetActive(false); */ 
            // for beard deactive

        return _currentPlayer;
    }

    private GameObject SetGender(int index)
    {
        //if (_currentPlayer)
            //Destroy(_currentPlayer);

        _currentPlayer = index % 2 == 1 ?
            SpawnPlayer(boyPrefab, true):
            SpawnPlayer(girlPrefab, false);

        playerSkinnedMeshRenderer = _currentPlayer.GetComponentsInChildren<SkinnedMeshRenderer>();
        playerMeshRenderer = _currentPlayer.GetComponentsInChildren<MeshRenderer>();

        return _currentPlayer;
    }

    private GameObject SpawnPlayer(GameObject player, bool isBoy)
    {
        this.isBoy = isBoy;

        var pos = player.transform.position + new Vector3(0, 0.1f);
        return Instantiate(player, pos, transform.rotation * Quaternion.Euler(0, 180f, 0));
    }

    private void SetClothes(GameObject currentPlayer, float thin, float musc)
    {
        if (currentPlayer.transform.GetChild(0).transform.Find("Clothes"))
            currentPlayer.transform.GetChild(0).transform.Find("Clothes").gameObject.SetActive(false);       

        var c = currentPlayer.transform.GetChild(0);
        
        var g = isBoy ? Instantiate(boyClothes.clothes[Random.Range(0, boyClothes.clothes.Length)], c): 
                        Instantiate(girlClothes.clothes[Random.Range(0, girlClothes.clothes.Length)], c);
        var s = g.GetComponent<SkinnedMeshRenderer>();

        s.SetBlendShapeWeight(0, thin);
        s.SetBlendShapeWeight(1, musc);
    }

    public void OnStart()
    {
        //GameStart(); // React callback
    }

    public void OnFinish()
    {
        GameOver(); // React callback
    }

    private void OnEnable() 
    {
        WebRequest.onLoadedScreenshot += OnFinish;
    }

    private void OnDisable() 
    {
        WebRequest.onLoadedScreenshot -= OnFinish;
    }
}

public class GenomeHelper
{
    public static List<int> GeneratorWithSubstring(string input)
    {
        List<int> genomeList = new List<int>();

        int stringToInt(string s, int startIndex, int length)
        {
            return Convert.ToInt32(s.Substring(startIndex, length));
        }

        if (genomeList.Any())
            genomeList.Clear();

        genomeList.Add(stringToInt(input, 0, 1)); // 0 body
        genomeList.Add(stringToInt(input, 1, 2)); // 1 eye color
        genomeList.Add(stringToInt(input, 3, 1)); // 2 skin color
        genomeList.Add(stringToInt(input, 4, 2)); // 3 hair color
        genomeList.Add(stringToInt(input, 6, 2)); // 4 faceHair color

        genomeList.Add(stringToInt(input, 8, 1)); // 5 hair
        genomeList.Add(stringToInt(input, 9, 1)); // 6 eyebrow
        genomeList.Add(stringToInt(input, 10, 1)); // 7 eye
        genomeList.Add(stringToInt(input, 11, 1)); // 8 ear
        genomeList.Add(stringToInt(input, 12, 1)); // 9 nose 
        genomeList.Add(stringToInt(input, 13, 1)); // 10 mouth
        genomeList.Add(stringToInt(input, 14, 1)); // 11 jaw

        return genomeList;
    }
}
